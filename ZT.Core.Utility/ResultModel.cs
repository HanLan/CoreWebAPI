﻿using System;

namespace ZT.Core.Utility
{
    /// <summary>
    /// 返回数据统一模型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResultModel<T>
    {
        /// <summary>
        /// 状态码 200为正常，ErrMsg为空，1000为保存返回错误信息 ErrMsg
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 异常信息
        /// </summary>
        public string ErrMsg { get; set; }
        /// <summary>
        /// 返回数据
        /// </summary>
        public T Data { get; set; }

        public ResultModel(T cdata)
        {
            Data = cdata;
            State = 200;
            ErrMsg = "";
        }

        public ResultModel(Exception ex)
        {
            var errmsg = string.Empty;
            errmsg += ex.Message;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                errmsg += ex.Message;
            }
            State = 1000;
            ErrMsg = errmsg;
            Data = default(T);
        }
    }
}
