﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Reflection;

namespace ZT.Core.Utility
{
    /// <summary>
    ///枚举显示[Description("")]中的值
    ///<remark>
    ///     示例 
    ///     [JsonConverter(typeof(EnumJsonConvert[EnumType]))]
    ///     public UserState StateStr { get; set; }
    ///</remark>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EnumJsonConvert<T> : JsonConverter where T : struct, IConvertible
    {
        /// <summary>
        /// 判断类型
        /// </summary>
        public void EnumJsonConverter()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T 必须是枚举类型");
            }
        }
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        bool IsNullableType(Type theType)
        {
            return (theType.IsGenericType && theType.
            GetGenericTypeDefinition().Equals
            (typeof(Nullable<>)));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }
            try
            {
                return reader.Value.ToString();
            }
            catch (Exception)
            {
                throw new Exception(string.Format("不能将枚举{1}的值{0}转换为Json格式.", reader.Value, objectType));
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            string bValue = value.ToString();
            int isNo;
            if (int.TryParse(bValue, out isNo))
            {
                bValue = GetEnumDescription(typeof(T), isNo);
            }
            else
            {
                bValue = GetEnumDescription(typeof(T), value.ToString());
            }


            writer.WriteValue(bValue);
        }

        /// <summary>
        /// 获取枚举描述
        /// </summary>
        /// <param name="type">枚举类型</param>
        /// <param name="value">枚举名称</param>
        /// <returns></returns>
        private string GetEnumDescription(Type type, string value)
        {
            try
            {
                FieldInfo field = type.GetField(value);
                if (field == null)
                {
                    return "";
                }

                var desc = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (desc != null) return desc.Description;

                return "";
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 获取枚举描述
        /// </summary>
        /// <param name="type">枚举类型</param>
        /// <param name="value">枚举hasecode</param>
        /// <returns></returns>
        private string GetEnumDescription(Type type, int value)
        {
            try
            {

                FieldInfo field = type.GetField(Enum.GetName(type, value));
                if (field == null)
                {
                    return "";
                }

                var desc = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (desc != null) return desc.Description;

                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}
