﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZT.Core.Utility
{
    public class EnumItem
    {
        /// <summary>
        /// 值
        /// </summary>
        public int Value { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Caption { get; set; }
     }
}
