﻿using System;

namespace ZT.Core.Model
{
    /// <summary>
    /// 审批日志表
    /// </summary>
    public class Flow_Log:ModelBase
    {
        /// <summary>
        /// 流程ID
        /// </summary>
        public Guid MasterId { get; set; }
        /// <summary>
        /// 节点ID
        /// </summary>
        public Guid PointId { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public Enums.FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 业务ID
        /// </summary>
        public Guid BusinessId { get; set; }
        /// <summary>
        /// 审批用户
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// 审批结果
        /// </summary>
        public Enums.FlowResult FlowResult { get; set; }
        /// <summary>
        /// 审批意见
        /// </summary>
        public string FlowMemo { get; set; }
        /// <summary>
        /// 发起人ID
        /// </summary>
        public Guid StartUserId { get; set; }
        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime FlowTime { get; set; }
        /// <summary>
        /// 审批版本
        /// </summary>
        public long FlowVersion { get; set; }
    }
}
