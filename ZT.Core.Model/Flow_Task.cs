﻿using System;


namespace ZT.Core.Model
{
    /// <summary>
    /// 审批任务
    /// </summary>
    public class Flow_Task:ModelBase
    {
        /// <summary>
        /// 流程ID
        /// </summary>
        public Guid MasterId { get; set; }
        /// <summary>
        /// 节点ID
        /// </summary>
        public Guid PointId { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public Enums.FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 审批用户ID
        /// </summary>
        public Guid UserId { get; set; } 
        /// <summary>
        /// 业务ID
        /// </summary>
        public Guid BusinessId { get; set; }
        /// <summary>
        /// 发起时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 发起人ID
        /// </summary>
        public Guid StartUserId { get; set; }
        /// <summary>
        /// 流程JSON
        /// </summary>
        public string FlowJSON { get; set; }
        /// <summary>
        /// 审批版本
        /// </summary>
        public long FlowVersion { get; set; }

    }
}
