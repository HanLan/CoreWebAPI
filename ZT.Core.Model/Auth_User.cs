﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZT.Core.Enums;

namespace ZT.Core.Model
{
    /// <summary>
    /// 用户
    /// </summary>
    public class Auth_User:ModelBase
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Pwd { get; set; }
        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// token过期时间
        /// </summary>
        public DateTime TokenValidTime { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public UserState State { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 是否提示
        /// </summary>
        public bool IsTip { get; set; } = true;
    }
}
