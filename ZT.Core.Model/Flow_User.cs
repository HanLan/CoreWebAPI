﻿using System;

namespace ZT.Core.Model
{
    /// <summary>
    /// 审批用户
    /// </summary>
    public class Flow_User:ModelBase
    {
        /// <summary>
        /// 审批主表ID
        /// </summary>
        public Guid MasterId { get; set; }
        /// <summary>
        ///节点ID
        /// </summary>
        public Guid PointId { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// 用户顺序
        /// </summary>
        public int UserIndex { get; set; }

    }
}
