﻿namespace ZT.Core.Model
{

    /// <summary>
    /// 审批流程定义主表
    /// </summary>

    public class Flow_Master:ModelBase
    {
        /// <summary>
        /// 审批流程名称
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public Enums.FlowBusinessType BusinessType { get; set; }
        ///// <summary>
        ///// 区域ID
        ///// </summary>
        //public Guid AreaID { get; set; }
        /// <summary>
        /// 流程说明
        /// </summary>
        public string FlowMemo { get; set; }
        /// <summary>
        /// 可撤消
        /// </summary>
        public bool AllowCancel { get; set; }
    }
}
