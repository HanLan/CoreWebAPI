﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZT.Core.Model
{
    /// <summary>
    /// 角色
    /// </summary>
    public class Auth_Role : ModelBase
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
         
    }
}
