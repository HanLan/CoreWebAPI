﻿using System;


namespace ZT.Core.Model
{
    /// <summary>
    /// 审批流程定义节点表
    /// </summary>
    public class Flow_Point:ModelBase
    {
        /// <summary>
        /// 主表ID
        /// </summary>
        public Guid MasterId { get; set; }
        /// <summary>
        /// 节点名称
        /// </summary>
        public string PointName { get; set; }
        /// <summary>
        /// 节点类型
        /// </summary>
        public Enums.FlowPointType PointType { get; set; }
        /// <summary>
        /// 是否必填审批意见
        /// </summary>
        public bool MustFlowMemo { get; set; }
        /// <summary>
        /// 节点顺序
        /// </summary>
        public int PointIndex { get; set; }


    }
}
