﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZT.Core.Model
{
    /// <summary>
    /// 系统日志
    /// </summary>
    public class Sys_Log:ModelBase
    {
        /// <summary>
        /// 客户端IP
        /// </summary> 
        public string ClientIP { get; set; }
        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestParam { get; set; }
        /// <summary>
        /// 请求方式
        /// </summary>
        public string RequestMethod { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string RequestAction { get; set; }
        /// <summary>
        /// 返回数据
        /// </summary>
        public string ResponseData { get; set; }  
        /// <summary>
        /// 日志用户
        /// </summary>
        public string LogUser { get; set; }
        /// <summary>
        /// 操作结果
        /// </summary>
        public bool OperateResult { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }
        /// <summary>
        /// 日志时间
        /// </summary>
        public DateTime LogDate { get; set; }

    }
}
