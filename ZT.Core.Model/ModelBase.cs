﻿using System;

namespace ZT.Core.Model
{
    /// <summary>
    /// 实体父类
    /// </summary>
    public class ModelBase
    {
        /// <summary>
        /// ID，主键
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();
  
    }
}
