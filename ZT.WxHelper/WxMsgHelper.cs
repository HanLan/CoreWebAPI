﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;

namespace ZT.WxHelper
{
    /// <summary>
    /// 微信信息解析/回复的帮助方式
    /// </summary>
    public class WxMsgHelper
    {


        #region "消息结构"
        /// <summary>
        /// Xml转化为class
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T XMLToObj<T>(XElement xml) where T : class
        {
            var tType = typeof(T);
            T re = (T)Activator.CreateInstance(tType);

            foreach (var p in tType.GetProperties())
            {
                XElement temp = xml.Element(p.Name);
                if (temp != null)
                {
                    if (p.PropertyType == typeof(string))
                    {
                        p.SetValue(re, temp.Value);
                    }
                    if (p.PropertyType == typeof(int))
                    {
                        p.SetValue(re, int.Parse(temp.Value));
                    }
                    if (p.PropertyType == typeof(decimal))
                    {
                        p.SetValue(re, decimal.Parse(temp.Value));
                    }
                    if (p.PropertyType == typeof(double))
                    {
                        p.SetValue(re, double.Parse(temp.Value));
                    }
                }
            }

            return re;
        }

        /// <summary>
        /// 文本消息结构
        /// {0}替换为：openID
        /// {1}替换为：appID
        /// {2}替换为：时间戳
        /// {3}替换为：Content 具体文本内容
        /// </summary>
        public const string WxMsgText = @"<xml> 
                                           <ToUserName><![CDATA[{0}]]></ToUserName>
                                           <FromUserName><![CDATA[{1}]]></FromUserName>
                                           <CreateTime>{2}</CreateTime>
                                           <MsgType><![CDATA[text]]></MsgType>
                                           <Content><![CDATA[{3}]]></Content>
                                           </xml>";
        /// <summary>
        /// 图片消息结构
        /// {0}替换为：openID
        /// {1}替换为：appID
        /// {2}替换为：时间戳
        /// {3}替换为：图片ID，素材管理
        /// </summary>
        public const string WxMsgImage = @"<xml>
                                           <ToUserName><![CDATA[{0}]]></ToUserName>
                                           <FromUserName><![CDATA[{1}]]></FromUserName>
                                           <CreateTime>{2}</CreateTime>
                                           <MsgType><![CDATA[image]]></MsgType>
                                           <Image><MediaId><![CDATA[{3}]]></MediaId></Image>
                                           </xml>";

        /// <summary>
        /// 图文消息
        /// {0}替换为 openID
        /// {1}替换为 appID
        /// {2}替换为是间戳
        /// {3}替换为图文条数
        /// {4}替换为 Item内容
        /// </summary>
        public const string WxMsgTextAndImage = @"<xml> 
                                                    <ToUserName><![CDATA[{0}]]></ToUserName 
                                                    <FromUserName><![CDATA[{1}]]></FromUserName> 
                                                    <CreateTime>{2}</CreateTime> 
                                                    <MsgType><![CDATA[news]]></MsgType> 
                                                    <ArticleCount>{3}</ArticleCount> 
                                                    <Articles>{4}</Articles> 
                                                    </xml>";

        /// <summary>
        /// 图文消息Item
        /// {0}替换为 Title标题
        /// {1}替换为 Descrption 描述
        /// {2}替换为 PicUrl图片的地址
        /// {3}替换为 Url 内容连接地址
        /// 
        /// </summary>
        public const string WxMsgTextAndImageItem = @"<item> 
                                                        <Title><![CDATA[{0}]]></Title>
                                                        <Description><![CDATA[{1}]]></Description>
                                                        <PicUrl><![CDATA[{2}]]></PicUrl>
                                                        <Url><![CDATA[{3}]]></Url>
                                                        </item>";



        #endregion

        /// <summary>
        /// 验证签名，确保信息的正确性
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="timestamp"></param>
        /// <param name="nonce"></param>
        /// <returns></returns>
        public static bool CheckSignaTrue(string signature, string timestamp, string nonce)
        {
            string token = WxConfig.Config.CheckToken;

            List<string> strList = new List<string>() {
                token,timestamp,nonce
            };
            strList.Sort();
            return Sha1Encrypt(string.Join("", strList)) == signature;

        }
        /// <summary>
        /// Sha1加密方式
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string Sha1Encrypt(string sourceString)
        {
            byte[] bytes = Encoding.Default.GetBytes(sourceString);
            HashAlgorithm hash = new SHA1CryptoServiceProvider();
            bytes = hash.ComputeHash(bytes);
            StringBuilder sb = new StringBuilder();
            foreach (var b in bytes)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }
        /// <summary>
        /// MD5加密方式
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string Md5Encrypt(string sourceString)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(sourceString);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");
            }
            return byte2String;
        }


        /**
      * 生成时间戳，标准北京时间，时区为东八区，自1970年1月1日 0点0分0秒以来的秒数
       * @return 时间戳
      */
        public static string GenerateTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }
    }
}
