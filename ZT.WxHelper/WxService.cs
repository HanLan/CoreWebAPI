﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZT.WxHelper
{
    /// <summary>
    /// 微信服务
    /// </summary>
    public class WxService
    {

        #region "菜单"
        /// <summary>
        /// 创建 菜单
        /// </summary>
        /// <param name="menu">菜单json</param>
        /// <returns></returns>
        public static Dto.WxResult<string> CreateMenu(Dto.WxMenu menu)
        {
            if (menu.button.Count() > 3)
            {
                throw new Exception("一级菜单菜单数超过3个不被允许");
            }
            if (menu.button.Select(s => s.sub_button.Count()).Any(q => q > 5))
            {
                throw new Exception("二级菜单数超过5个不被允许");
            }

            var re = WxAction.WxAPIPOST(WxConfig.WxActionType.CreateMenu, JsonConvert.SerializeObject(menu));


            var robj = (JObject)JsonConvert.DeserializeObject(re);
            var msg = robj.GetValue("errmsg").ToString();
            if (msg == "ok")
            {
                return new Dto.WxResult<string>("ok");
            }
            else
            {
                return new Dto.WxResult<string>(new Exception(msg));
            }
        }
        /// <summary>
        /// 查询菜单
        /// </summary>
        /// <returns></returns>
        public static Dto.WxResult<Dto.WxMenuQuery> GetMenu()
        {
            var url = string.Format(WxConfig.WxUrlQueryMenu, Access_TokenModel.access_Token);
            var re = WxAction.WxAPIGet(WxConfig.WxActionType.Other, "", "", url);
            if (re.Contains("errcode"))
            {
                var robj = (JObject)JsonConvert.DeserializeObject(re);
                var msg = robj.GetValue("errmsg").ToString();
                return new Dto.WxResult<Dto.WxMenuQuery>(new Exception(msg));
            }
            else
            {
                var reobj = JsonConvert.DeserializeObject<Dto.WxMenuQuery>(re);
                return new Dto.WxResult<Dto.WxMenuQuery>(reobj);
            }
        }
        #endregion

        /// <summary>
        /// 用户信息
        /// </summary>
        /// <param name="openid">openid</param>
        /// <returns></returns>
        public static Dto.WxResult<Dto.WxUserInfo> UserInfo(string openid)
        {
            var re = WxAction.WxAPIGet(WxConfig.WxActionType.Userinfo, openid);
            if (re.Contains("errcode"))
            {
                var robj = (JObject)JsonConvert.DeserializeObject(re);
                var msg = robj.GetValue("errmsg").ToString();

                return new Dto.WxResult<Dto.WxUserInfo>(new Exception(msg));
            }
            else
            {
                var reobj = JsonConvert.DeserializeObject<Dto.WxUserInfo>(re);
                return new Dto.WxResult<Dto.WxUserInfo>(reobj);
            }
        }

        /// <summary>
        /// 发送模板信息
        /// </summary>
        /// <param name="mmsg"></param>
        /// <returns></returns>
        public static Dto.WxResult<string> SendTemplateMessage(Dto.TemplateMessagePara mmsg)
        {
            var url = string.Format(WxConfig.WxUrlSendTemplateMessage, Access_TokenModel.access_Token);
            var para = JsonConvert.SerializeObject(mmsg);
            var restr = WxAction.WxAPIPOST(WxConfig.WxActionType.Other, para, url);
            var robj = (JObject)JsonConvert.DeserializeObject(restr);
            if (robj["errcode"].ToString() == "0")
            {
                return new Dto.WxResult<string>(robj["msgid"].ToString());
            }
            else
            {
                return new Dto.WxResult<string>(new Exception(robj["errmsg"].ToString()));
            }
        }
    }
}
