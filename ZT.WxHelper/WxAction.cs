﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZT.WxHelper
{
    public class WxAction
    {

        /// <summary>
        /// 调用微信API,get请求
        /// <remark>
        ///  type 4 获取openid;1 获取用户信息
        /// </remark>
        /// </summary>
        /// <param name="type">方法类型</param>
        /// <param name="openID">OpenID</param>
        /// <param name="code"></param>
        /// <param name="geturl"></param>
        /// <returns></returns>
        public static string WxAPIGet(WxConfig.WxActionType type, string openID = "", string code = "", string geturl = "")
        {
            string url = "";
            switch (type)
            {
                case WxConfig.WxActionType.GetOpenID:
                    url = string.Format(WxConfig.WxUrlGetOpenID, WxConfig.Config.AppID, WxConfig.Config.AppSecret, code);
                    break;
                case WxConfig.WxActionType.Userinfo:
                    url = string.Format(WxConfig.WxUrlUserInfo, Access_TokenModel.access_Token, openID);
                    break;
                default:
                    url = geturl;
                    break;
            }
            System.Net.WebRequest request = (System.Net.WebRequest)System.Net.HttpWebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            string re = "";
            using (System.Net.WebResponse response = request.GetResponse())
            {
                if (response == null)
                {
                    throw new Exception("Response is not Created");
                }
                using (System.IO.Stream stream = response.GetResponseStream())
                {
                    using (System.IO.StreamReader rd = new System.IO.StreamReader(stream, System.Text.Encoding.UTF8))
                    {
                        re = rd.ReadToEnd();
                        rd.Close();
                    }
                    stream.Close();
                }
            }
            return re;
        }

        /// <summary>
        /// 调用微信API,post请求
        /// <remark>
        ///  type 4 获取openid;1 获取用户信息
        /// </remark>
        /// </summary>
        /// <param name="type">方法类型</param>
        /// <param name="ParameterJson"></param>
        /// <param name="posturl"></param>
        /// <returns></returns>
        public static string WxAPIPOST(WxConfig.WxActionType type, string ParameterJson = "", string posturl = "")
        {
            string url = "";
            switch (type)
            {
                case WxConfig.WxActionType.CreateMenu:
                    url = string.Format(WxConfig.WxUrlCreateMenu, Access_TokenModel.access_Token);
                    break;
                case WxConfig.WxActionType.SetIndustry:
                    url = string.Format(WxConfig.WxUrlSetIndustry, Access_TokenModel.access_Token);
                    break;
                case WxConfig.WxActionType.SendRedPack:
                    url = WxConfig.WxUrlSendRedPack;
                    break;
                default:
                    url = posturl;
                    break;
            }
            System.Net.WebRequest request = (System.Net.WebRequest)System.Net.HttpWebRequest.Create(url);
            request.Method = "POST";
            Byte[] postbytes = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(ParameterJson);
            request.ContentType = "application/x-www-form-urlencded";
            request.ContentLength = postbytes.Length;
            using (System.IO.Stream stream = request.GetRequestStream())
            {
                stream.Write(postbytes, 0, postbytes.Length);
                stream.Close();
            }
            string re = "";
            using (System.Net.WebResponse response = request.GetResponse())
            {
                if (response == null)
                {
                    throw new Exception("Response is not Created");
                }
                using (System.IO.Stream restream = response.GetResponseStream())
                {
                    using (System.IO.StreamReader getrd = new System.IO.StreamReader(restream, System.Text.Encoding.UTF8))
                    {
                        re = getrd.ReadToEnd();
                        getrd.Close();
                    }
                    restream.Close();
                }
            }
            return re;
        }
    }

    
}
