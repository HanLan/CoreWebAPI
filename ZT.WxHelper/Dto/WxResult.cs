﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZT.WxHelper.Dto
{
    /// <summary>
    /// 同一返回数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class WxResult<T>
    {
        public WxResult() { }

        public WxResult(T data)
        {
            Data = data;
            State = 200;
            ErrMsg = "";
        }

        public WxResult(Exception ex)
        {
            var errmsg = string.Empty;
            errmsg += ex.Message;
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                errmsg += ex.Message;
            }
            State = 1000;
            ErrMsg = errmsg;
            Data = default(T);
        }
        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// 状态 200，1000
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrMsg { get; set; }
    }
}
