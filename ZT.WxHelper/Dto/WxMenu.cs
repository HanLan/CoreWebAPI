﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZT.WxHelper.Dto
{
    /// <summary>
    /// 微信菜单
    /// </summary>
    public class WxMenuBase
    {
        /// <summary>
        /// 类型：click  view 
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 菜单的catpoal
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 路由
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// key值
        /// </summary>
        public string key { get; set; }
    }

    /// <summary>
    /// 菜单项
    /// </summary>
    public class WxMenuItem : WxMenuBase
    {
        public IEnumerable<WxMenuBase> sub_button { get; set; }
        public WxMenuItem()
        {
            sub_button = new List<WxMenuBase>();
        }
    }

    /// <summary>
    /// 创建菜单参数
    /// </summary>
    public class WxMenu
    {
        public IEnumerable<WxMenuItem> button { get; set; }
    }

    public class WxMenuQuery
    {
        public WxMenu Menu { get; set; }
    }

}
