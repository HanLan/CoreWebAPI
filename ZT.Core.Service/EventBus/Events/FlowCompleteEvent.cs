﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZT.Core.Service.EventBus
{
    public class FlowCompleteEvent:IEvent
    {
        /// <summary>
        /// 业务类型
        /// </summary>
        public Enums.FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 业务ID
        /// </summary>
        public Guid BusinessID { get; set; }
        /// <summary>
        /// 0 开始 1 通过 2 拒绝  9撤销
        /// </summary>
        public Enums.FlowResult FlowResult { get; set; }
        /// <summary>
        /// 审批意见
        /// </summary>
        public string FlowMemo { get; set; }
        /// <summary>
        /// 审批用户
        /// </summary>
        public Guid UserID { get; set; }
    }
}
