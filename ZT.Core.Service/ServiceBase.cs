﻿using System;
using ZT.Core.IService;
using ZT.Core.ORM;

namespace ZT.Core.Service
{
    public class ServiceBase:IServiceBase
    {
        /// <summary>
        /// 工作单元
        /// </summary>
        public IUnitOfWork UnitOfWork { get; set; }
        /// <summary>
        ///进入系统的用户
        /// </summary>
        public Dto.AppUser AppUser  { get; set; }

        /// <summary>
        /// 事件触发后的回调，这里不处理回调方法
        /// </summary>
        /// <param name="flowevent"></param>
        /// <param name="result"></param>
        /// <param name="ex"></param>
        public virtual void FlowCallBack(EventBus.FlowCompleteEvent flowevent, bool result, Exception ex) { }

    }
}
