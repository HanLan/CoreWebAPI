﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using ZT.Core.IService;
using ZT.WxHelper;

namespace ZT.Core.Service
{
    public class WechatService : ServiceBase, IWechat
    {
        public string WxResInfo(XElement resv)
        {
            var msgbase = WxMsgHelper.XMLToObj<WxHelper.Dto.MessageBase>(resv);
            string responseXML = string.Empty;
            switch (msgbase.MsgType)
            {
                case "event":
                    var evbase = WxMsgHelper.XMLToObj<WxHelper.Dto.EventMessageBase>(resv);
                    switch (evbase.Event)
                    {
                        case "subscribe":
                            responseXML = string.Format(WxHelper.WxMsgHelper.WxMsgText,evbase.FromUserName,WxHelper.WxConfig.Config.WXCode, WxHelper.WxMsgHelper.GenerateTimeStamp(),"谢谢您关注公众号!");
                            break;
                        case "unsubscribe":
                            break;
                        case "LOCATION"://地理位置
                           var LocationEvenMsg = WxMsgHelper.XMLToObj<WxHelper.Dto.LocationEvenMessage>(resv);

                            break;
                    }
                    break;
                default:
                    break;
            }
            return responseXML;
        }
    }
}
