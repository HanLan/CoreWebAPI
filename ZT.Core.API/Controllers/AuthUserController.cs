﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ZT.Core.Dto;
using ZT.Core.IService;
using ZT.Core.MiddleWare;
using ZT.Core.Utility;


namespace ZT.Core.API.Controllers
{

    /// <summary>
    /// 用户权限
    /// </summary>
    [Route("api/authUser")]
    [ApiController]
    public class AuthUserController : BaseController
    {
        private IAuthUser bll = null;

        /// <summary>
        /// 用户权限构造
        /// </summary>
        /// <param name="auth"></param>
        public AuthUserController(IAuthUser auth)
        {
            bll = auth;
            Services.Add(auth);

        }
        /// <summary>
        /// 新建角色
        /// </summary>
        /// <param name="roleNew"></param>
        /// <returns></returns>
        [HttpPost,Route("addRole")]
        public ResultModel<bool> AddRole(RoleNew roleNew)
        {
    
            return bll.AddRole(roleNew);
        }
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet,Route("delRole/{roleId:Guid}")]
        public ResultModel<bool> DelRole(Guid roleId)
        {
         
            return bll.DelRole(roleId);
        }
        /// <summary>
        /// 用户注销
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("userLogout")]
        public ResultModel<bool> UserLogout()
        {
            return bll.UserLogout();
        }

        /// <summary>
        /// 根据登录的用户获取菜单
        /// </summary>
        /// <returns></returns>
        [HttpGet,Route("getMenuByLogin")]
        public ResultModel<IEnumerable<MenuView>> GetMenuByLogin()
        {
            return bll.GetMenuByLogin();
        }
        /// <summary>
        /// 根据角色ID获取权限
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet,Route("getRoleAuth/{roleId:Guid}")]
        public ResultModel<IEnumerable<AuthView>> GetRoleAuth(Guid roleId)
        {
            return bll.GetRoleAuth(roleId);
        }
        /// <summary>
        /// 设置用户角色
        /// </summary>
        /// <param name="setRoles"></param>
        /// <returns></returns>
        [HttpPost,Route("setLoginRole")]
        public ResultModel<bool> SetLoginRole(SetLoginRoles setRoles)
        {
           
            return bll.SetLoginRole(setRoles);
        }
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="signin"></param>
        /// <returns></returns>
        [AllowAnymous]
        [HttpPost, Route("userLogin")]
        public ResultModel<UserInfo> UserLogin(UserSignin signin)
        {
            RequestParam = JsonConvert.SerializeObject(signin);
            Memo = "用户登录";
            return bll.UserLogin(signin);
        }

        /// <summary>
        /// 获取用户Profile
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("getUserProfile")]
        public ResultModel<UserProfile> GetUserProfile()
        {
            return bll.GetUserProfile();
        }
        /// <summary>
        /// 设置用户Profile
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        [HttpPost, Route("setProfile")]
        public ResultModel<bool> SetProfile(SetUserProfile para)
        {
            return bll.SetProfile(para);
        }
        /// <summary>
        /// 新建一个用户
        /// </summary>
        /// <param name="userNew"></param>
        /// <returns></returns>
        [HttpPost, Route("addUserInfo")]
        public ResultModel<bool> AddUserInfo(AuthUserNew userNew)
        {
            return bll.AddUserInfo(userNew);
        }
        /// <summary>
        /// 根据ID删除用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet, Route("delAuthUser/{userId:Guid}")]
        public ResultModel<bool> DelAuthUser(Guid userId)
        {
            return bll.DelAuthUser(userId);
        }
        /// <summary>
        /// 分页获取用户信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost, Route("getUserPages")]
        public ResultModel<Pager<UserPage>> GetUserPages(PagerQuery<UserPageQuery> query)
        {
            return bll.GetUserPages(query);
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="pwdChange"></param>
        /// <returns></returns>
        [HttpPost, Route("changePwd")]
        public ResultModel<bool> ChangePwd(UserPwdChange pwdChange)
        {
            return bll.ChangePwd(pwdChange);
        }
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet, Route("resetPwd/{userId:Guid}")]
        public ResultModel<bool> ResetPwd(Guid userId)
        {
            return bll.ResetPwd(userId);
        }
        /// <summary>
        /// 不再提示
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("notTip")]
        public ResultModel<bool> NotTip()
        {
            return bll.NotTip();
        }
        /// <summary>
        /// 设置角色权限
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("setRoleAuth")]
        public ResultModel<bool> SetRoleAuth(RoleAuthSet roleAuth)
        {
            return bll.SetRoleAuth(roleAuth);
        }
    }
}
