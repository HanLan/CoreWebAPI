﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ZT.Core.Dto;
using ZT.Core.IService;
using ZT.Core.MiddleWare;
using ZT.Core.Utility;

namespace SSFund.Core.API.Controllers
{
    /// <summary>
    /// 审批流程
    /// </summary>
    [Route("api/flow")]
    [ApiController]
    public class FlowController:BaseController
    {
        private IFlow bll = null;
        /// <summary>
        /// 流程构造
        /// </summary>
        /// <param name="flow"></param>
        public FlowController(IFlow flow)
        {
            bll = flow;
            Services.Add(flow);
        }
        /// <summary>
        /// 新建流程主表
        /// </summary>
        /// <param name="flowMaster"></param>
        /// <returns></returns>
        [HttpPost, Route("addFlowMaster")]
        public ResultModel<bool> AddFlowMaster(AddFlowMaster flowMaster)
        {
           
            return bll.AddFlowMaster(flowMaster);
        }
        /// <summary>
        /// 撤销了流程
        /// </summary>
        /// <param name="cancelParameter"></param>
        /// <returns></returns>
        [HttpPost, Route("cancelFlow")]
        public ResultModel<bool> CancelFlow(FlowCancelParameter cancelParameter)
        {
          
            return bll.CancelFlow(cancelParameter);
        }
        /// <summary>
        /// 删除了流程主表
        /// </summary>
        /// <param name="flowMasterId"></param>
        /// <returns></returns>
        [HttpGet, Route("delFlowMaster/{flowMasterId:Guid}")]
        public ResultModel<bool> DelFlowMaster(Guid flowMasterId)
        {
           
            return bll.DelFlowMaster(flowMasterId);
        }
        /// <summary>
        /// 修改了流程主表
        /// </summary>
        /// <param name="flowMaster"></param>
        /// <returns></returns>
        [HttpPost, Route("editFlowMaster")]
        public ResultModel<bool> EditFlowMaster(EditFlowMaster flowMaster)
        {
           
            return bll.EditFlowMaster(flowMaster);
        }
        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="executeParameter"></param>
        /// <returns></returns>
        [HttpPost, Route("flowExecute")]
        public ResultModel<bool> FlowExecute(FlowExecuteParameter executeParameter)
        {
          
            return bll.FlowExecute(executeParameter);
        }
        /// <summary>
        /// 获取审批业务类型
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("getBusinessType")]
        public ResultModel<IEnumerable<EnumItem>> GetBusinessType()
        {
            return bll.GetBusinessType();
        }

        /// <summary>
        /// 审批日志
        /// </summary>
        /// <param name="businessId"></param>
        /// <returns></returns>
        [HttpGet, Route("getFlowLogs/{businessId:Guid}")]
        public ResultModel<IEnumerable<FlowLogView>> GetFlowLogs(Guid businessId)
        {
            return bll.GetFlowLogs(businessId);
        }
        /// <summary>
        /// 获取审批主表模型
        /// </summary>
        /// <param name="flowMasterId"></param>
        /// <returns></returns>
        [HttpGet, Route("getFlowMasterModel/{flowMasterId:Guid}")]
        public ResultModel<FlowMasterModel> GetFlowMasterModel(Guid flowMasterId)
        {
            return bll.GetFlowMasterModel(flowMasterId);
        }
        /// <summary>
        /// 分页获取审批主表模型
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost, Route("getFlowMasterPages")]
        public ResultModel<Pager<FlowMasterPage>> GetFlowMasterPages(PagerQuery<FlowQuery> query)
        {
            return bll.GetFlowMasterPages(query);
        }
        /// <summary>
        /// 根据主表ID获取审批节点
        /// </summary>
        /// <param name="flowMasterId"></param>
        /// <returns></returns>
        [HttpGet, Route("getFlowMasterPoints/{flowMasterId:Guid}")]
        public ResultModel<FlowMasterPointView> GetFlowMasterPoints(Guid flowMasterId)
        {
            return bll.GetFlowMasterPoints(flowMasterId);
        }
        /// <summary>
        /// 获取我参与的审批任务
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost, Route("getFlowMyPartakePages")]
        public ResultModel<Pager<FlowTaskHisPage>> GetFlowMyPartakePage(PagerQuery<FlowQuery> query)
        {
            return bll.GetFlowMyPartakePage(query);
        }
        /// <summary>
        /// 我发起的审批任务
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost, Route("getFlowMyStartPages")]
        public ResultModel<Pager<FlowTaskHisPage>> GetFlowMyStartPage(PagerQuery<FlowQuery> query)
        {
            return bll.GetFlowMyStartPage(query);
        }
        /// <summary>
        /// 我的审批任务
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost, Route("getFlowMyTaskPages")]
        public ResultModel<Pager<FlowMyTaskPage>> GetFlowMyTaskPage(PagerQuery<FlowQuery> query)
        {
            return bll.GetFlowMyTaskPage(query);
        }
        /// <summary>
        /// 保存审批节点
        /// </summary>
        /// <param name="flowPoint"></param>
        /// <returns></returns>
        [HttpPost, Route("saveFlowPoint")]
        public ResultModel<bool> SaveFlowPoint(FlowPointParameter flowPoint)
        {
            return bll.SaveFlowPoint(flowPoint);
        }
        /// <summary>
        /// 发起审批
        /// </summary>
        /// <param name="startParameter"></param>
        /// <returns></returns>
        [HttpPost, Route("startFlow")]
        public ResultModel<bool> StartFlow(FlowStartParameter startParameter)
        {
            return bll.StartFlow(startParameter);
        }
    }
}