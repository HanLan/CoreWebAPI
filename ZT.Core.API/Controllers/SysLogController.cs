﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ZT.Core.Dto;
using ZT.Core.IService;
using ZT.Core.MiddleWare;
using ZT.Core.Utility;

namespace ZT.Core.API.Controllers
{
    /// <summary>
    /// 系统日志
    /// </summary>
    [Route("api/sysLog")]
    [ApiController]
    public class SysLogController : BaseController
    {
        private ISysLog bll = null;
        public SysLogController(ISysLog sysLog)
        {
            bll = sysLog;
            Services.Add(sysLog);
        }
        /// <summary>
        /// 根据Id，删除系统日志
        /// </summary>
        /// <param name="logId"></param>
        /// <returns></returns>
        [HttpGet,Route("delSysLogById/{logId:Guid}")]
        public ResultModel<bool> DelSysLogById(Guid logId)
        {
            RequestParam = JsonConvert.SerializeObject(logId);
            Memo = "根据Id，删除系统日志";
            return bll.DelSysLogById(logId);
        }
        /// <summary>
        /// 根据Id,获取系统日志
        /// </summary>
        /// <param name="logId"></param>
        /// <returns></returns>
        [HttpGet, Route("getSysLogById/{logId:Guid}")]
        public ResultModel<SysLogModel> GetSysLogById(Guid logId)
        {
            RequestParam = JsonConvert.SerializeObject(logId);
            Memo = "根据Id,获取系统日志";
            return bll.GetSysLogById(logId);
        }
        /// <summary>
        /// 分页获取系统日志，关键词:用户包含的词
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost, Route("getSysLogPages")]
        public ResultModel<Pager<SysLogPage>> GetSysLogPages(PagerQuery<SysLogQuery> query)
        {
            RequestParam = JsonConvert.SerializeObject(query);
            Memo = "分页获取系统日志，关键词:用户包含的词";
            return bll.GetSysLogPages(query);
        }
    }
}