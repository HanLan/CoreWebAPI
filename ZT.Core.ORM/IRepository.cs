﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ZT.Core.ORM
{
    /// <summary>
    /// 数据仓储接口，增删改查
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T :Model.ModelBase
    {
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool Any(Guid ID);
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool Any(Expression<Func<T, bool>> predicate);
        /// <summary>
        /// 新建 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        T Insert(T entity);
        /// <summary>
        /// 新建
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Insert(IEnumerable<T> entity);
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        T Update(T entity);
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        T GetModel(Guid ID);

        T GetModel(Expression<Func<T, bool>> pridicate);
        /// <summary>
        /// 条件查询
        /// </summary>
        /// <param name="pridicate"></param>
        /// <returns></returns>
        IQueryable<T> Queryable(Expression<Func<T, bool>> pridicate = null);

        bool Delete(T entity);

        bool Delete(Expression<Func<T, bool>> predicate);


    }
}
