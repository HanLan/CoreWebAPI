﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZT.Core.ORM
{
    public interface IUnitOfWork:IDisposable
    {
        /// <summary>
        /// 提交操作
        /// </summary>
        /// <returns></returns>
        bool Commit();
        /// <summary>
        /// 实例化具体的仓储
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IRepository<T> Repository<T>() where T : Model.ModelBase;
    }
}
