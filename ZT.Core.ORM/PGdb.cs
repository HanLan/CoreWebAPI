﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZT.Core.Model;

namespace ZT.Core.ORM
{
    public class PGdb:DbContext
    {

        public PGdb(DbContextOptions<PGdb> options) : base(options) { }

        DbSet<Auth_User> Auth_User { get; set; }
        DbSet<Auth_UserProfile> Auth_UserProfile { get; set; }
        DbSet<Auth_Key> Auth_Key { get; set; }
        DbSet<Auth_KeyDetail> Auth_KeyDetail { get; set; }
        DbSet<Auth_Role> Auth_Role { get; set; }
        DbSet<Auth_RoleAuthScope> Auth_RoleAuthScope { get; set; }
        DbSet<Auth_UserRole> Auth_UserRole { get; set; }
        DbSet<Flow_Master> Flow_Master { get; set; }
        DbSet<Flow_Point> Flow_Point { get; set; }
        DbSet<Flow_Task> Flow_Task { get; set; }
        DbSet<Flow_User> Flow_User { get; set; }
        DbSet<Flow_Log> Flow_Log { get; set; }

        DbSet<Sys_Log> Sys_Log { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //pg的uuid与系统的GUID
            modelBuilder.HasPostgresExtension("uuid-ossp");
            base.OnModelCreating(modelBuilder);
        }
    }
}
