﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ZT.Core.Enums
{
    /// <summary>
    /// 性别
    /// <remark>
    /// 0 女
    /// 1 男
    /// </remark>
    /// </summary>
    public enum GenderType
    {
        /// <summary>
        /// 女
        /// </summary>
        [Description("女")]
        FeMale = 0,
        /// <summary>
        /// 男
        /// </summary>
        [Description("男")]
        Male = 1
    }
}
