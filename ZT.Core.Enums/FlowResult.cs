﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZT.Core.Enums
{
    /// <summary>
    /// 审批结果
    /// <remark>
    /// 0 已提交
    /// 1 通过
    /// 2 拒绝
    /// 9 撤销
    /// </remark>
    /// </summary>
    public enum FlowResult
    {

        /// <summary>
        /// 已提交
        /// </summary>
        [Description("已提交")]
        Started = 0,
        /// <summary>
        /// 通过
        /// </summary>
        [Description("通过")]
        Pass =1,
        /// <summary>
        /// 拒绝
        /// </summary>
        [Description("拒绝")]
        Deny =2,
        /// <summary>
        /// 撤消
        /// </summary>
        [Description("撤销")]
        Cancel =9
    }
}
