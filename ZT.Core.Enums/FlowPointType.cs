﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ZT.Core.Enums
{
    /// <summary>
    /// 审批节点类型
    /// <remark>
    /// 1 单人
    /// 2 顺序多人
    /// 3 并行多人
    /// </remark>
    /// </summary>
    public enum FlowPointType
    {
        /// <summary>
        /// 单人
        /// </summary>
        [Description("单人")]
        Single = 1,
        /// <summary>
        /// 顺序多人
        /// </summary>
        [Description("顺序多人")]
        OrderMulti = 2,
        /// <summary>
        /// 并行多人
        /// </summary>
        [Description("并行多人")]
        MustMulti = 3
    }
}
