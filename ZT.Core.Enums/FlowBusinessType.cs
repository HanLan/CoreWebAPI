﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ZT.Core.Enums
{
    /// <summary>
    /// 业务审批流程类型
    /// <Remark>
    /// 0 未定义
    /// 1 测试
    /// </Remark>
    /// </summary>
    public enum FlowBusinessType
    {
        /// <summary>
        /// 未定义
        /// </summary>
        [Description("未定义")]
        Undefined,
        /// <summary>
        /// 测试
        /// </summary>
        [Description("测试")]
        Test,

    }
}
