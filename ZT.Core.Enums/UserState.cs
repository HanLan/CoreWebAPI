﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ZT.Core.Enums
{
    /// <summary>
    /// 用户状态
    /// <remark>
    /// 1 正常
    /// 2 作废
    /// </remark>
    /// </summary>
    public enum UserState
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        Normal = 1,
        /// <summary>
        /// 作废
        /// </summary>
        [Description("作废")]
        Cancel = 2,
    }
}
