﻿using System;
using System.Collections.Generic;
using System.Text;
using ZT.Core.Dto;
using ZT.Core.Utility;

namespace ZT.Core.IService
{
    public interface IFlow
    {
        /// <summary>
        /// 新建流程主表模型
        /// </summary>
        /// <param name="flowMaster"></param>
        /// <returns></returns>
        ResultModel<bool> AddFlowMaster(AddFlowMaster flowMaster);
        /// <summary>
        /// 删除流程主表模型
        /// </summary>
        /// <param name="flowMasterId"></param>
        /// <returns></returns>
        ResultModel<bool> DelFlowMaster(Guid flowMasterId);
        /// <summary>
        /// 修改流程主表模型
        /// </summary>
        /// <param name="flowMaster"></param>
        /// <returns></returns>
        ResultModel<bool> EditFlowMaster(EditFlowMaster flowMaster);
        /// <summary>
        /// 获取流程主表模型
        /// </summary>
        /// <param name="flowMasterId"></param>
        /// <returns></returns>
        ResultModel<FlowMasterModel> GetFlowMasterModel(Guid flowMasterId);
        /// <summary>
        /// 分页获取流程主表模型
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        ResultModel<Pager<FlowMasterPage>> GetFlowMasterPages(PagerQuery<FlowQuery> query);
        /// <summary>
        /// 我的审批任务
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        ResultModel<Pager<FlowMyTaskPage>> GetFlowMyTaskPage(PagerQuery<FlowQuery> query);
        /// <summary>
        /// 我发起的审批
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        ResultModel<Pager<FlowTaskHisPage>> GetFlowMyStartPage(PagerQuery<FlowQuery> query);
        /// <summary>
        /// 我参与的审批
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        ResultModel<Pager<FlowTaskHisPage>> GetFlowMyPartakePage(PagerQuery<FlowQuery> query);
        /// <summary>
        /// 获取流程主表节点集合
        /// </summary>
        /// <param name="flowMasterId"></param>
        /// <returns></returns>
        ResultModel<FlowMasterPointView> GetFlowMasterPoints(Guid flowMasterId);
        /// <summary>
        /// 根据业务ID获取日志
        /// </summary>
        /// <param name="bussinessId"></param>
        /// <returns></returns>
        ResultModel<IEnumerable<FlowLogView>> GetFlowLogs(Guid businessId);
        /// <summary>
        /// 获取业务流程类型
        /// </summary>
        /// <returns></returns>
        ResultModel<IEnumerable<EnumItem>> GetBusinessType();
        /// <summary>
        /// 保存审批流程节点
        /// </summary>
        /// <param name="flowPoint"></param>
        /// <returns></returns>
        ResultModel<bool> SaveFlowPoint(FlowPointParameter flowPoint);
        /// <summary>
        /// 发起审批流程
        /// </summary>
        /// <param name="startParameter"></param>
        /// <returns></returns>
        ResultModel<bool> StartFlow(FlowStartParameter startParameter);
        /// <summary>
        /// 审批
        /// </summary>
        /// <param name="executeParameter"></param>
        /// <returns></returns>
        ResultModel<bool> FlowExecute(FlowExecuteParameter executeParameter);
        /// <summary>
        /// 撤销审批流程
        /// </summary>
        /// <param name="cancelParameter"></param>
        /// <returns></returns>
        ResultModel<bool> CancelFlow(FlowCancelParameter cancelParameter);
    }
}
