﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace ZT.Core.IService
{
    public interface IWechat
    {
        string WxResInfo(XElement resv);
    }
}
