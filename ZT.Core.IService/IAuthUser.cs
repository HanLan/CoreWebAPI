﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using ZT.Core.Dto;
using ZT.Core.Utility;

namespace ZT.Core.IService
{
    public interface IAuthUser
    {
        /// <summary>
        /// 根据登陆用户获取菜单
        /// </summary>
        /// <returns></returns>
        ResultModel<IEnumerable<MenuView>> GetMenuByLogin();
        /// <summary>
        /// 根据角色ID获取权限
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        ResultModel<IEnumerable<AuthView>> GetRoleAuth(Guid roleId);
        /// <summary>
        /// 退出系统
        /// </summary>
        /// <returns></returns>
        ResultModel<bool> UserLogout();
        /// <summary>
        /// 新建角色
        /// </summary>
        /// <param name="roleNew"></param>
        /// <returns></returns>
        ResultModel<bool> AddRole(RoleNew roleNew);
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        ResultModel<bool> DelRole(Guid roleId);
        /// <summary>
        /// 设置用户角色
        /// </summary>
        /// <param name="setRoles"></param>
        /// <returns></returns>
        ResultModel<bool> SetLoginRole(SetLoginRoles setRoles);
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="signin"></param>
        /// <returns></returns>
        ResultModel<UserInfo> UserLogin(UserSignin signin);
        /// <summary>
        /// 获取用户Profile
        /// </summary>
        /// <returns></returns>
        ResultModel<UserProfile> GetUserProfile();
        /// <summary>
        /// 设置用户Profile
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        ResultModel<bool> SetProfile(SetUserProfile para);
        /// <summary>
        /// 新建一个用户
        /// </summary>
        /// <param name="userNew"></param>
        /// <returns></returns>
        ResultModel<bool> AddUserInfo(AuthUserNew userNew);
        /// <summary>
        /// 根据ID删除用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultModel<bool> DelAuthUser(Guid userId);
        /// <summary>
        /// 分页获取用户信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        ResultModel<Pager<UserPage>> GetUserPages(PagerQuery<UserPageQuery> query);
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="pwdChange"></param>
        /// <returns></returns>
        ResultModel<bool> ChangePwd(UserPwdChange pwdChange);
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        ResultModel<bool> ResetPwd(Guid UserId);
        /// <summary>
        /// 不再提示
        /// </summary>
        /// <returns></returns>
        ResultModel<bool> NotTip();
        /// <summary>
        /// 设置角色权限
        /// </summary>
        /// <returns></returns>
        ResultModel<bool> SetRoleAuth(RoleAuthSet roleAuth);
    }
}
