﻿using System;
using System.Collections.Generic;
using System.Text;
using ZT.Core.Dto;
using ZT.Core.Utility;

namespace ZT.Core.IService
{
    /// <summary>
    /// 系统日志
    /// </summary>
    public interface ISysLog
    {
        /// <summary>
        /// 删除日志，根据Id
        /// </summary>
        /// <param name="logId"></param>
        /// <returns></returns>
        ResultModel<bool> DelSysLogById(Guid logId);
        /// <summary>
        ///获取日志模型
        /// </summary>
        /// <param name="logId"></param>
        /// <returns></returns>
        ResultModel<SysLogModel> GetSysLogById(Guid logId);
        /// <summary>
        /// 分页查询系统日志
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        ResultModel<Pager<SysLogPage>> GetSysLogPages(PagerQuery<SysLogQuery> query);
    }
}
