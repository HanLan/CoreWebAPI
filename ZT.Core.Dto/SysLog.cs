﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZT.Core.Dto
{
    /// <summary>
    /// 日志分页数据
    /// </summary>
    public class SysLogPage
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 客户端IP
        /// </summary> 
        public string ClientIP { get; set; }
        /// <summary>
        /// 请求方式
        /// </summary>
        public string RequestMethod { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string RequestAction { get; set; }
        /// <summary>
        /// 日志用户
        /// </summary>
        public string LogUser { get; set; }
        /// <summary>
        /// 操作结果
        /// </summary>
        public bool OperateResult { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }
        /// <summary>
        /// 日志时间
        /// </summary>
        public DateTime LogDate { get; set; }
    }
    /// <summary>
    /// 分页查询条件
    /// </summary>
    public class SysLogQuery
    {
        /// <summary>
        /// 起始日期
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 关键词:日志用户
        /// </summary>
        public string KeyWord { get; set; }
    }
    /// <summary>
    /// 系统日志模型
    /// </summary>
    public class SysLogModel
    {
        /// <summary>
        /// 客户端IP
        /// </summary> 
        public string ClientIP { get; set; }
        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestParam { get; set; }
        /// <summary>
        /// 请求方式
        /// </summary>
        public string RequestMethod { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string RequestAction { get; set; }
        /// <summary>
        /// 返回数据
        /// </summary>
        public string ResponseData { get; set; }
        /// <summary>
        /// 日志用户
        /// </summary>
        public string LogUser { get; set; }
        /// <summary>
        /// 操作结果
        /// </summary>
        public bool OperateResult { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }
        /// <summary>
        /// 日志时间
        /// </summary>
        public DateTime LogDate { get; set; }
    }
}
