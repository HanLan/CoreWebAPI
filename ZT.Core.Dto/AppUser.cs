﻿using System;
using System.Collections.Generic;
using System.Text;
using ZT.Core.Model;

namespace ZT.Core.Dto
{
    /// <summary>
    /// 进入系统的用户
    /// </summary>
    public class AppUser
    {
        /// <summary>
        /// 用户信息
        /// </summary>
        public Auth_User UserInfo { get; set; }
        /// <summary>
        /// 用户档案
        /// </summary>
        public Auth_UserProfile UserProfile { get; set; }
    }
}
