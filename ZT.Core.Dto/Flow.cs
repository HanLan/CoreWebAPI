﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using ZT.Core.Enums;
using ZT.Core.Utility;

namespace ZT.Core.Dto
{

    /// <summary>
    /// 新建流程主表
    /// </summary>
    public class AddFlowMaster
    {
        ///// <summary>
        ///// 区域ID
        ///// </summary>
        //public Guid AreaID { get; set; }
        /// <summary>
        /// 审批流程名称
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 流程说明
        /// </summary>
        public string FlowMemo { get; set; }
        /// <summary>
        /// 可撤消
        /// </summary>
        public bool AllowCancel { get; set; }
    }
    /// <summary>
    /// 修改流程主表
    /// </summary>
    public class EditFlowMaster 
    {
        /// <summary>
        /// ID,主键
        /// </summary>
        public Guid ID { get; set; }
        /// <summary>
        /// 审批流程名称
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 流程说明
        /// </summary>
        public string FlowMemo { get; set; }

    }
    /// <summary>
    /// 流程主表模型
    /// </summary>
    public class FlowMasterModel
    {
        /// <summary>
        /// ID,主键
        /// </summary>
        public Guid ID { get; set; }
        /// <summary>
        /// 审批流程名称
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 流程说明
        /// </summary>
        public string FlowMemo { get; set; }
        /// <summary>
        /// 可撤消
        /// </summary>
        public bool AllowCancel { get; set; }
    }
    /// <summary>
    /// 流程主表分页模型
    /// </summary>
    public class FlowMasterPage
    {
        /// <summary>
        /// ID，主键
        /// </summary>
        public Guid ID { get; set; }
        /// <summary>
        /// 审批流程名称
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        [JsonConverter(typeof(EnumJsonConvert<FlowBusinessType>))]
        public FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 流程说明
        /// </summary>
        public string FlowMemo { get; set; }
        /// <summary>
        /// 可撤消
        /// </summary>
        public bool AllowCancel { get; set; }
    }
    /// <summary>
    /// 我的审批
    /// </summary>
    public class FlowMyTaskPage
    {
        /// <summary>
        /// 审批任务ID
        /// </summary>
        public Guid TaskId { get; set; }
        /// <summary>
        /// 业务ID
        /// </summary>
        public Guid BusinessId { get; set; }
        /// <summary>
        /// 流程名
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        [JsonConverter(typeof(EnumJsonConvert<FlowBusinessType>))]
        public FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 发起用户
        /// </summary>
        public string StartFlowUser { get; set; }
        /// <summary>
        /// 审批流程是否完成
        /// </summary>
        public bool IsComplete { get; set; }
    }
    /// <summary>
    /// 我发起的或参与的
    /// </summary>
    public class FlowTaskHisPage
    {
        /// <summary>
        /// 业务ID
        /// </summary>
        public Guid BusinessId { get; set; }
        /// <summary>
        /// 流程名
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        [JsonConverter(typeof(EnumJsonConvert<FlowBusinessType>))]
        public FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 发起用户
        /// </summary>
        public string StartFlowUser { get; set; }
        /// <summary>
        /// 审批流程是否完成
        /// </summary>
        public bool IsComplete { get; set; }
    }
    /// <summary>
    /// 审批日志
    /// </summary>
    public class FlowLogView
    {
        /// <summary>
        /// 审批版本
        /// </summary>
        public long FlowVersion { get; set; }
        /// <summary>
        /// 节点详情列表
        /// </summary>
        public IEnumerable<FlowLogDetail> Points { get; set; }
    }
    /// <summary>
    /// 审批日志详情
    /// </summary>
    public class FlowLogDetail
    {
        /// <summary>
        /// 节点名称
        /// </summary>
        public string PointName { get; set; }
        /// <summary>
        /// 审批用户
        /// </summary>
        public string UserCaption { get; set; }
        /// <summary>
        /// 审批结果
        /// </summary>
        [JsonConverter(typeof(EnumJsonConvert<FlowResult>))]
        public FlowResult FlowResult { get; set; }
        /// <summary>
        /// 审批意见
        /// </summary>
        public string FlowMemo { get; set; }
        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime FlowTime { get; set; }

    }
    /// <summary>
    /// 审批节点
    /// </summary>
    public class FlowMasterPointView
    {
        /// <summary>
        /// 流程主表ID
        /// </summary>
        public Guid MasterID { get; set; }
        /// <summary>
        /// 流程名
        /// </summary>
        public string FlowName { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        [JsonConverter(typeof(EnumJsonConvert<FlowBusinessType>))]
        public FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 节点详情
        /// </summary>
        public IEnumerable<FlowPointDetail> PointDetails { get; set; }

    }
    /// <summary>
    /// 流程节点
    /// </summary>
    public class FlowPointDetail
    {
        /// <summary>
        /// 节点名
        /// </summary>
        public string PointName { get; set; }
        /// <summary>
        /// 节点类型
        /// </summary>
        [JsonConverter(typeof(EnumJsonConvert<FlowPointType>))]
        public FlowPointType PointType { get; set; }
        /// <summary>
        /// 是否必填审批意见
        /// </summary>
        public bool MustFlowMemo { get; set; }
        /// <summary>
        /// 节点顺序
        /// </summary>
        public int PointIndex { get; set; }
        /// <summary>
        /// 节点审批人集合
        /// </summary>
        public IEnumerable<PointUser> PointUsers { get; set; }
    }
    /// <summary>
    /// 审批用户详情
    /// </summary>
    public class PointUser
    {
        /// <summary>
        /// 审批人ID
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// 审批人名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 审批人顺序
        /// </summary>
        public int UserIndex { get; set; }
    }

    /// <summary>
    /// 流程主表查询条件
    /// </summary>
    public class FlowQuery
    {
        /// <summary>
        /// 关键词，流程名
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// 业务类型
        /// </summary>
        public FlowBusinessType BussinessType { get; set; }
    }

    /// <summary>
    /// 新建审批流程节点参数
    /// </summary>
    public class FlowPointParameter
    {
        /// <summary>
        /// 流程主表ID
        /// </summary>
        public Guid MasterId { get; set; }
        /// <summary>
        /// 审批节点
        /// </summary>
        public IEnumerable<MasterPointDetail>  MasterPoints { get; set; }
    }
    /// <summary>
    /// 主体节点详情
    /// </summary>
    public class MasterPointDetail
    {
        /// <summary>
        /// 节点名称
        /// </summary>
        public string PointName { get; set; }
        /// <summary>
        /// 节点类型
        /// </summary>
        public FlowPointType PointType { get; set; }
        /// <summary>
        /// 是否必填审批意见
        /// </summary>
        public bool MustFlowMemo { get; set; }
        /// <summary>
        /// 节点顺序
        /// </summary>
        public int PointIndex { get; set; }
        /// <summary>
        /// 审批用户
        /// </summary>
        public IEnumerable<FlowUser> FlowUsers { get; set; }
    }
    /// <summary>
    /// 审批人
    /// </summary>
    public class FlowUser
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// 用户顺序
        /// </summary>
        public int UserIndex { get; set; }
    }
    /// <summary>
    /// 发起审批流程参数
    /// </summary>
    public class FlowStartParameter
    {
        /// <summary>
        /// 业务类型
        /// </summary>
        public Enums.FlowBusinessType BusinessType { get; set; }
        /// <summary>
        /// 业务ID
        /// </summary>
        public Guid BusinessId { get; set; }
    }
    /// <summary>
    /// 审批参数
    /// </summary>
    public class FlowExecuteParameter
    {
        /// <summary>
        /// 审批任务ID
        /// </summary>
        public Guid TaskId { get; set; }
        /// <summary>
        /// 审批结果
        /// </summary>
        public FlowResult FlowResult { get; set; }
        /// <summary>
        /// 审批意见
        /// </summary>
        public string FlowMemo { get; set; }
    }
    /// <summary>
    /// 撤销审批参数
    /// </summary>
    public class FlowCancelParameter
    {
        /// <summary>
        /// 业务ID
        /// </summary>
        public Guid BusinessId { get; set; }
        /// <summary>
        /// 撤消原因
        /// </summary>
        public string CancelMemo { get; set; }
    }
    /// <summary>
    /// 流程Json模型
    /// </summary>
    public class FlowJSONModel
    {
        /// <summary>
        /// 审批流程
        /// </summary>
        public Model.Flow_Master Flow_Master { get; set; }
        /// <summary>
        /// 审批节点集合
        /// </summary>
        public IEnumerable<Model.Flow_Point> Flow_Point { get; set; }
        /// <summary>
        /// 审批用户集合
        /// </summary>
        public IEnumerable<Model.Flow_User> Flow_User { get; set; }
    }

}
