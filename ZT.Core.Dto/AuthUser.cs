﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using ZT.Core.Utility;

namespace ZT.Core.Dto
{

    /// <summary>
    /// 菜单
    /// </summary>
    public class MenuView
    {
        /// <summary>
        /// 模块名
        /// </summary>
        public string ModuleName { get; set; }
        /// <summary>
        /// Icon
        /// </summary>
        public string ImgUrl { get; set; } 
        /// <summary>
        /// 菜单项
        /// </summary>
        public IEnumerable<MenuDetail> MenuDetails { get; set; }
    }
    /// <summary>
    /// 菜单详情
    /// </summary>
    public class MenuDetail
    {
        /// <summary>
        /// 菜单名
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 路由地址
        /// </summary>
        public string RoutUrl { get; set; }
    }
    /// <summary>
    /// 权限
    /// </summary>
    public class AuthView
    {
        /// <summary>
        /// 模块名
        /// </summary>
        public string ModuleName { get; set; }
        /// <summary>
        /// 权限项
        /// </summary>
        public IEnumerable<AuthDetails> AuthDetails { get; set; }
    }
    /// <summary>
    /// 权限项
    /// </summary>
    public class AuthDetails
    {
        /// <summary>
        /// 菜单名
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 权限项详情
        /// </summary>
        public IEnumerable<AuthFunc>  AuthFuncs { get; set; }

    }
    /// <summary>
    /// 权限项详情
    /// </summary>
    public class AuthFunc
    {
        /// <summary>
        /// 功能名
        /// </summary>
        public string FuncName { get; set; }
        /// <summary>
        /// 功能Key
        /// </summary>
        public string FuncKey { get; set; }
        /// <summary>
        /// 是否中
        /// </summary>
        public bool IsChecked { get; set; }
    }
    /// <summary>
    /// 用户登陆
    /// </summary>
    public class UserSignin
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Pwd { get; set; }
    }
    /// <summary>
    /// 用户信息
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// 用户
        /// </summary>
        public AuthUser AuthUser { get; set; }
        /// <summary>
        /// 个人资料
        /// </summary>
        public UserProfile UserProfile { get; set; }
    }
    /// <summary>
    /// 用户
    /// </summary>
    public class AuthUser
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 是否提醒
        /// </summary>
        public bool IsTip { get; set; }
    }
    /// <summary>
    /// 用户资料
    /// </summary>
    public class UserProfile
    {
        /// <summary>
        /// 文件资料
        /// </summary>
        public Guid Id{ get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string RelName { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadImg { get; set; }
    }
    /// <summary>
    /// 用户资料
    /// </summary>
    public class SetUserProfile
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string RelName { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadImg { get; set; }
    }
    /// <summary>
    /// 新建用户
    /// </summary>
    public class AuthUserNew
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string RelName { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
    }
    /// <summary>
    /// 用户信息
    /// </summary>
    public class UserPage
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string RelName { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadImg { get; set; }
        /// <summary>
        /// 用户状态
        /// </summary>
        [JsonConverter(typeof(EnumJsonConvert<Enums.UserState>))]
        public Enums.UserState UserState { get; set; }
    }
    /// <summary>
    /// 用户信息查询条件
    /// </summary>
    public class UserPageQuery
    {
        /// <summary>
        /// 关键词（用户名、姓名）
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// 用户状态
        /// </summary>
        public Enums.UserState UserState { get; set; }
    }

    /// <summary>
    /// 新建角色
    /// </summary>
    public class RoleNew
    {
        /// <summary>
        /// 角色名
        /// </summary>
        public string RoleName { get; set; }
    }

    /// <summary>
    /// 用户设置角色
    /// </summary>
    public class SetLoginRoles
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public IEnumerable<Guid> RoleIds { get; set; }
    }

    /// <summary>
    /// 修改
    /// </summary>
    public class UserPwdChange
    {
        /// <summary>
        /// 旧密码
        /// </summary>
        public string OldPwd { get; set; }
        /// <summary>
        /// 新密码
        /// </summary>
        public string Pwd { get; set; }
    }


    /// <summary>
    /// 设置角色的权限
    /// </summary>
    public class RoleAuthSet
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public Guid RoleId { get; set; }
        /// <summary>
        /// 权限KEYS
        /// </summary>
        public IEnumerable<string> AuthKeys { get; set; }
    }
}
